/*
 *  DS1307 test
 */

#include <16F819.h>

#fuses MCLR,INTRC_IO,NOWDT,NOPROTECT,NOBROWNOUT,NOPUT
#use delay(clock=4MHz)

#use rs232(baud=9600, xmit=PIN_B5, STREAM=PC)

#include "ds1307.c"


void main() {
  ds1307_init();
  delay_ms(100);

  //ds1307_set_date_time(17, )
  byte sec, min, hour, day, month, year, dow;   // Last one is day of week

  //ds1307_set_date_time(17, 01, 18, 3, 11, 50, 00);

  while(1) {
    ds1307_get_date(day, month, year, dow);
    ds1307_get_time(hour, min, sec);
    fprintf(PC, "\r\n%d-%d-%d: %d:%d:%d", year, month, day, hour, min, sec);
    delay_ms(1000);
  }
}
