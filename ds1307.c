/*
 *  DS1307 driver
 *  Source: https://www.ccsinfo.com/forum/viewtopic.php?t=23255
 *  (Modified slightly)
 */

#include "ds1307.h"
#use i2c(master, sda=I2C_SDA, scl=I2C_SCL)

void ds1307_init(void) {
  /*
   *  Initialise RTC
   */
  byte seconds = 0;

  i2c_start();
  i2c_write(RTC_W_ADDRESS);       // WR to RTC
  i2c_write(0x00);                // REG 0
  i2c_start();
  i2c_write(RTC_R_ADDRESS);       // RD from RTC
  seconds = bcd2bin(i2c_read(0)); // Read current "seconds" in DS1307
  i2c_stop();
  seconds &= 0x7F;

  delay_us(3);

  i2c_start();
  i2c_write(RTC_W_ADDRESS);       // WR to RTC
  i2c_write(0x00);                // REG 0
  i2c_write(bin2bcd(seconds));    // Start oscillator with current "seconds value
  i2c_start();
  i2c_write(RTC_W_ADDRESS);       // WR to RTC
  i2c_write(0x07);                // Control Register
  i2c_write(0x80);                // Disable squarewave output pin
  i2c_stop();

}

void ds1307_set_date_time(byte day, byte mth, byte year, byte dow, byte hr, byte min, byte sec) {
  /*
   *  Set the RTC's date and time
   *  Needs to convert each number to binary-coded decimal for the RTC
   */
  sec &= 0x7F;
  hr &= 0x3F;

  i2c_start();
  i2c_write(RTC_W_ADDRESS);     // I2C write address
  i2c_write(0x00);              // Start at REG 0 - Seconds
  i2c_write(bin2bcd(sec));      // REG 0
  i2c_write(bin2bcd(min));      // REG 1
  i2c_write(bin2bcd(hr));       // REG 2
  i2c_write(bin2bcd(dow));      // REG 3
  i2c_write(bin2bcd(day));      // REG 4
  i2c_write(bin2bcd(mth));      // REG 5
  i2c_write(bin2bcd(year));     // REG 6
  i2c_write(0x80);              // REG 7 - Disable squarewave output pin
  i2c_stop();
}

void ds1307_get_date(byte &day, byte &mth, byte &year, byte &dow) {
  /*
   *  Get only the current date from the RTC
   *  Needs to be passed pointers to store the results
   *  Results are converted from BCD to binary
   */
  i2c_start();
  i2c_write(RTC_W_ADDRESS);
  i2c_write(0x03);            // Start at REG 3 - Day of week
  i2c_start();
  i2c_write(RTC_R_ADDRESS);
  dow  = bcd2bin(i2c_read() & 0x7f);   // REG 3
  day  = bcd2bin(i2c_read() & 0x3f);   // REG 4
  mth  = bcd2bin(i2c_read() & 0x1f);   // REG 5
  year = bcd2bin(i2c_read(0));            // REG 6
  i2c_stop();
}

void ds1307_get_time(byte &hr, byte &min, byte &sec) {
  /*
   *  Get only the current time from the RTC
   *  Results converted from BCD to binary
   */
  i2c_start();
  i2c_write(RTC_W_ADDRESS);
  i2c_write(0x00);            // Start at REG 0 - Seconds
  i2c_start();
  i2c_write(RTC_R_ADDRESS);
  sec = bcd2bin(i2c_read() & 0x7f);
  min = bcd2bin(i2c_read() & 0x7f);
  hr  = bcd2bin(i2c_read(0) & 0x3f);
  i2c_stop();

}

byte bin2bcd(byte binary_value) {
  /*
   *  Convert binary number to binary-coded decimal
   */
  byte temp;
  byte retval;

  temp = binary_value;
  retval = 0;

  while(1)
  {
    // Get the tens digit by doing multiple subtraction
    // of 10 from the binary value.
    if(temp >= 10)
    {
      temp -= 10;
      retval += 0x10;
    }
    else // Get the ones digit by adding the remainder.
    {
      retval += temp;
      break;
    }
  }

  return(retval);
}


byte bcd2bin(byte bcd_value) {
  /*
   *  Convert binary-coded decimal to binary number
   *  Input range 00 to 99
   */
  byte temp;

  temp = bcd_value;
  // Shifting upper digit right by 1 is same as multiplying by 8.
  temp >>= 1;
  // Isolate the bits for the upper digit.
  temp &= 0x78;

  // Now return: (Tens * 8) + (Tens * 2) + Ones
  return(temp + (temp >> 2) + (bcd_value & 0x0f));
}
