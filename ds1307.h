/*
 *  DS1307 driver header file
 *  Contains constants and function definitions
 */

#define I2C_SDA PIN_B1
#define I2C_SCL PIN_B4
#define RTC_W_ADDRESS 0xD0
#define RTC_R_ADDRESS 0xD1

// Function prototypes
void ds1307_init(void);
void ds1307_set_date_time(byte day, byte mth, byte year, byte dow, byte hr, byte min, byte sec);
void ds1307_get_date(byte &day, byte &mth, byte &year, byte &dow);
void ds1307_get_time(byte &hr, byte &min, byte &sec);
byte bin2bcd(byte binary_value);
byte bcd2bin(byte bcd_value);
